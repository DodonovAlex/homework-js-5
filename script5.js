// Теоретичні питання

// 1. Опишіть своїми словами, що таке метод об'єкту

// Метод об'єкту – це дія, яку можна виконувати над об'єктом шляхом визначення певної функції. За допомогою функцій,
// переглядаються або змінюються властивості об'єкта.

// ----------------------------------------------------------------------------------------------------------

// 2. Який тип даних може мати значення властивості об'єкта?

// У якості властивості об'єкту може виступати будь-якій з наступних вісьми типів даних (від a до h):
// a) Number - цілі числа;
// b) String - рядок, символьний тип даних;
// c) Boolean - логічний тип даних: true і false;
// d) Масиви - набори значень з ключами;
// e) Об'єкти - сукупність пов'язаних данных та/також функціональних можливостей;
//    Зазвичай складаються з декількох змінних та функцій;
// f) Символи «symbol»;
// g) Undefined - змінна якій не присвоїли значення;
// h) Null - нулєве значення, "пусте значення".

// ----------------------------------------------------------------------------------------------------------

// 3. Об'єкт це посилальний тип даних. Що означає це поняття?

// Поняття "посилальний тип даних" означає, що об'єкт - це посилання на перелік значень та їх властивостей
// задля певного об'єкту.

//============================================================================================================

// Завдання

// Реалізувати функцію створення об'єкта "юзер". Завдання має бути виконане на чистому Javascript без використання
// бібліотек типу jQuery або React.

// Технічні вимоги:

// Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// При виклику функція повинна запитати ім'я та прізвище.
// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в
// нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль
// результат виконання функції.

// Необов'язкове завдання підвищеної складності

// Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму. Створити функції-сеттери setFirstName() та setLastName(), які дозволять змінити дані властивості.

function createNewUser() {
  firstName = prompt("Please enter Your name:", "Alex");
  lastName = prompt("Please enter Your surname:", "Dmitriyenko");

  alert("Hello, " + firstName + " " + lastName);

  return firstName, lastName;
}

createNewUser();

let newUser = {
  firstName: firstName,
  lastName: lastName,
  get login() {
    let string = `${firstName.substring(0, 1) + lastName.toLowerCase()}`;
    login = `${string.toLowerCase()}`;
    return login;
  },
  set login(login) {},
};

console.log(newUser);
console.log(newUser.login);

Object.defineProperty(newUser, "firstName", {
  writable: false,
  enumerable: false,
});

Object.defineProperty(newUser, "lastName", {
  writable: false,
  enumerable: false,
});

newUser.firstName = "Ivan";
newUser.lastName = "Sidorenko";
console.log(newUser);

function setFirstName() {
  Object.defineProperty(newUser, "firstName", {
    writable: true,
    enumerable: true,
  });
}

function setLastName() {
  Object.defineProperty(newUser, "lastName", {
    writable: true,
    enumerable: true,
  });
}

setFirstName();
setLastName();

newUser.firstName = "Ivan";
newUser.lastName = "Sidorenko";
console.log(newUser);
